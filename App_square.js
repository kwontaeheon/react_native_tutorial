/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

/* const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

class test2 extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
       <Text> test2 component </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
}); */


export default class gitbookTest extends Component {
  render() {
    return (
       <View style={styles2.container}>
        <NavBar />
        <Body />
       </View>
    );
  }
}

class NavBar extends Component {
  render() {
    return (
      <View style={styles2.navBar}>
          <Text>
            NavBar
          </Text>
      </View>
    );
  }
}

class Body extends Component {
  render() {
    return ( <View style={styles2.body}>
        <View style={styles2.left}>
        </View>
        <View style={styles2.right}>
          <View style={styles2.up}>
          </View>
        <View style={styles2.down}>
          </View>
        </View>
      </View>
    );
  }
}

const styles2 = StyleSheet.create( {
   container : {
     flex : 1,
     flexDirection : 'column',
   },
   navBar: {
    height : 80,
    backgroundColor: '#B0B0B0',
    justifyContent: 'center',
    alignItems: 'center',
   },
   body : {
     flex: 1,
     flexDirection:'row'
   },
   left: {
     flex : 1,
     backgroundColor: 'red',
   },
   right: {
     flex: 2,
     flexDirection : 'column',
   },
   up: {
     flex: 1,
     backgroundColor: 'blue',
   },
   down: {
     flex: 1,
     backgroundColor: 'yellow',
   }


   } ) ;

