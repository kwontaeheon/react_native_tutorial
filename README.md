# react_native_tutorial


## (참고) react-native 공식사이트 
https://facebook.github.io/react-native/docs/getting-started

## 실행 방법  (MacOS)

```
$ brew install node
$ brew install watchman
$ npm install -g react-native-cli
$ cd [PROJECT_FOLDER]
# iOS 
$ react-native run-ios 
# Android
$ react-native run-android
--> 실패할 경우 
$ sudo react-native run-android 
```

```
# Android  
https://developer.android.com/studio/
 Android Studio 설치 (아래 컴포넌트들 포함)
Android SDK
Android SDK Platform
Performance (Intel ® HAXM)
Android Virtual Device

avdmanager list avd

#If you don't have any devices listed from adb devices You'll want to start by creating an Android Virtual Device image.

$  android create avd -n <name> -t <target> --abi default/x86_64
<name> can be anything, such as react, <target> should be one of the options produced by the below command...

$  android list target
If that listing is empty, use the android command's GUI to install a target platform and such.

When you have the platform, and the device, you can then boot the device with emulator:

$  emulator -avd <name>


$/Users/kwontaeheon/Library/Android/sdk//tools/bin/avdmanager create avd --name rn-v2 -k "system-images;android-27;google_apis;x86"

# EMULATOR 실행 
$ANDROID_HOME/emulator/emulator -avd rn-v2

# 이후에 가능함
react-native run-android

# 이미 설치됐다는 오류 발생시
com.android.ddmlib.InstallException: INSTALL_FAILED_UPDATE_INCOMPATIBLE: 
Package com.gitbooktest signatures do not match the previously installed version; ignoring!
아래처럼 삭제후 실행
adb uninstall com.gitbooktest
```