import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import User from './components/User.js'
import NavBar from './components/navBar.js'
import ButtonGroup from './components/ButtonGroup.js'
import Taps from './components/Taps.js'





export default class gitbookTest extends Component {
  render() {
    return (
      <View style={styles.container}>
        <NavBar />
        <User />
        <View style={{height: 40}}>
        </View>
        <ButtonGroup />
        <View style={{flex: 1}}>
        </View>
        <Taps />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  }
});